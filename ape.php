<?php

require_once("animal.php");
class Ape extends animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "false";

    public function __construct($string)
    {
        $this->name = $string;
    }

    function yell()
    {
        echo "Auoooo";
    }
}
