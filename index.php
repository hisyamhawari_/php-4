<?php

require_once("Animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");

echo "Animal Name : " . $sheep->name . "<br>";
echo "Animal Legs : " . $sheep->legs . "<br>";
echo "Hot Blooded Animal : " . $sheep->cold_blooded . "<br><br>";

$kodok = new Frog("buduk");

echo "Animal Name :" . $kodok->name . "<br>";
echo "Animal Legs :" . $kodok->legs . "<br>";
echo "Hot Blooded Animal :" . $kodok->cold_blooded . "<br><br>";
$kodok->jump();


$sungokong = new Ape("kera sakti");

echo "Animal Name :" . $sungokong->name . "<br>";
echo "Animal Legs :" . $sungokong->legs . "<br>";
echo "Hot Blooded Animal :" . $sungokong->cold_blooded . "<br><br>";
$sungokong->yell();
