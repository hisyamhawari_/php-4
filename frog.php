<?php

require_once("animal.php");
class Frog extends animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "false";

    public function __construct($string)
    {
        $this->name = $string;
    }

    function jump()
    {
        echo "HOP HOP";
    }
}
